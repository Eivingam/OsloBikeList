# OsloBikeList
Prints a list of bike stations Oslo Citybike has, telling how many free bikes and docks each place has. 

## Instructions:
Runs best on python3, tested on Python 3.6.9 and 2.7.16

```sh
  Open a terminal
  Navigate to the folder containing the project
  run 'pip install -r requirements.txt'
  * If python3 is standard
  python realtimeBike.py
  * If python2 is standard
  python3 realtimeBike.py
```

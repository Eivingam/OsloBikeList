import requests
import json

stationInfoUrl = "https://gbfs.urbansharing.com/oslobysykkel.no/station_information.json"
stationStatusUrl = "https://gbfs.urbansharing.com/oslobysykkel.no/station_status.json"

params = "Client-Identifier: JobApplicant"


def getApiData(url, params):
    response = requests.get(url,params)
    if not response:
        print("Request failed", response.status_code)
        exit()
    else:
        data = response.json()
        return data


stationInfoList = getApiData(stationInfoUrl, params)['data']['stations']
stationInfoListSorted = sorted(stationInfoList, key = lambda k: k['station_id'])

stationStatusList = getApiData(stationStatusUrl, params)['data']['stations']
stationStatusListSorted = sorted(stationStatusList, key = lambda k: k['station_id'])

if not len(stationInfoListSorted) == len(stationStatusListSorted):
        print("Sanity check failed")
        exit()

for station in range(0, len(stationInfoListSorted)-1):
    stationName = stationInfoListSorted[station]['name']
    bikes = stationStatusListSorted[station]['num_bikes_available']
    docks = stationStatusListSorted[station]['num_docks_available']
    print(stationName+': '+'Available bikes:',bikes,'Available docks:',docks)

